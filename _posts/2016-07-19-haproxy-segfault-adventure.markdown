---
layout: post
title:  "a tale of HAProxy, segfaults, Docker, and gdb from production"
date:   2016-07-19 7:00:00
tags: microservices devops haproxy docker postmortem udacity
---

Published on Medium: [an HAProxy, segfaulted, choose-your-own stack frame adventure](https://engineering.udacity.com/haproxy-segfault-adventure-18cc1a7e1171)
